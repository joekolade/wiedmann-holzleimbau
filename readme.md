# Holzleimbau Wiedmann

## Description

A simple fronted for the company. There is no CMS involved.
Concept and Design by Markus Ruf. Developed by John Brown (der.jbrown@gmail.com)

## Dev Guide

1. Clone the project
2. Run `npm install`
3. Run `grunt serve` to serve the page locally and run Sass every time you hit the save button.
4. To build the app run `grunt dist` or simply `grunt`. The site for production will end up in the `\dist` folder
5. To clear the dist folder and the temp files run `grunt clean`.

Enjoy!
