/**
 * @author: John Brown, der.jbrown@gmail.com
 */

'use strict';

$(document).ready(function(){
	var resObject = new XMLHttpRequest();
	var output = $('#mytarget');
	var startContent = output.html();

	function sendReq() {
		resObject.open('get', 'inhalt.json', true);
		resObject.onreadystatechange = handleResponse;
		resObject.send(null);
	}

	function handleResponse() {
		if(resObject.readyState === 4) {
			var meinJSONObject = JSON.parse(resObject.responseText);
			//console.log(meinJSONObject.bindings[0].heading);
			//console.log(meinJSONObject);
			var outstring =
				'<h3><span class="neg">'
				+ meinJSONObject.bindings[0].heading
				+ '</span></h3>'
				+ '<div class="row">'
				+ '<div class="col-sm-6">'
				+ '<p>'
				+ meinJSONObject.bindings[0].text
				+ '</p>'
				+ '</div>'
				+ '<div class="col-sm-6">'
				+ '<img src="' + meinJSONObject.bindings[0].bild + '">'
				+ '</div>'
				+ '</div>'
				+ '<button id="zurueck">Zurück</button>';
			output.html(outstring);

		}
	}

	function backToStart() {
		output.html(startContent);
	}

	$('#clicky').click(function(){
		sendReq();
	});


});
